//
//  ViewController.swift
//  singleImageFromUrl
//
//  Created by Taslima Roya on 7/21/19.
//  Copyright © 2019 Taslima Roya. All rights reserved.
//

import UIKit
enum KittenImageLocation: String{
    case https="https://upload.wikimedia.org/wikipedia/commons/thumb/0/06/Kitten_in_Rizal_Park%2C_Manila.jpg/460px-Kitten_in_Rizal_Park%2C_Manila.jpg"}

class ViewController: UIViewController {
    
    
    @IBOutlet weak var imageView: UIImageView!
    let imageLocation=KittenImageLocation.https.rawValue
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func handleLoadButtonPress(_ sender: Any) {
        guard let imageUrl=URL(string: imageLocation) else{
            print("cannot create url")
            return
        }
        let task=URLSession.shared.dataTask(with: imageUrl) { (data, response, error) in
            guard let data=data else{
                print("no data ,or there was an error")
                return
            }
            let downloadedImage=UIImage(data: data)
            DispatchQueue.main.async {
                self.imageView.image=downloadedImage
            }
            
        }
        task.resume()
    }
    
}

